//
//  CurrentWeatherData.swift
//
//  Created by Jeroen Dunselman on 18/12/2016.
//  Copyright © 2016 Jeroen Dunselman. All rights reserved.
//

import Foundation
import Alamofire

class CurrentWeatherData {
    //url for current weather
    private let openWeatherMapBaseURL = "http://api.openweathermap.org/data/2.5/weather"
    private let openWeatherMapAPIKey = "f253a82b395b623b4473abb0ba4804c5"
    
    var lat: String?
    var lon: String?
    private var _cityName: String?

    private var _date: Double?
    private var _temp: String?
    private var _location: String?
    private var _weather: String?
    private var _humidity: String?
    private var _rain: String?
    private var _pressure: String?
    private var _windSpeed: String?
    private var _windDegrees: String?
    typealias JSONStandard = Dictionary<String, AnyObject>
    
    var temp: String {
        return _temp ?? "0 °C"
    }
    
    var humidity: String {
        return _humidity ?? "0 %"
    }
    
    var rain: String {
//        MARK: - todo: unfake it
        return _rain ?? "0.2 mm"
    }
    
    var pressure: String {
        return _pressure ?? "0 hPa"
    }
    
    var location: String {
        return _cityName ?? "Location Unknown"
    }
    
    var weather: String {
        return _weather ?? "n.a."//"Weather Not Available"
    }
    
    var windSpeed: String {
        return _windSpeed ?? "0 km/h"
    }

    var windDegrees: String {
        return _windDegrees ?? "0 °"
    }
    
    func setCoordinates(lat:String, lon:String) {
        self.lat = lat
        self.lon = lon
    }
    
    func downloadData(completed: @escaping ()-> ()) {
        let url = "\(openWeatherMapBaseURL)?lat=\(self.lat!)&lon=\(self.lon!)&APIKEY=\(openWeatherMapAPIKey)&units=metric&"
        Alamofire.request(url).responseJSON(
            completionHandler: {
                response in
                let result = response.result
                
                //wind, humidity, temp
                if let dict = result.value as? JSONStandard,
                    let main = dict["main"] as? JSONStandard,
                    let temp = main["temp"] as? Double,
                    let humidity = main["humidity"] as? Double,
                    let pressure = main["pressure"] as? Double,
                    let city = dict["name"] as? String,
                    let weatherArray = dict["weather"] as? [JSONStandard],
                    let weather = weatherArray[0]["main"] as? String,
                    let windArray = dict["wind"] as? NSDictionary,
                    let windSpeed = windArray["speed"] as? Double,
                    let windDegrees = windArray["deg"] as? Double
                {
                    self._cityName = city
                    self._temp = String(format: "%.0f °C", temp)// - 273.15)
                    self._weather = weather
                    let perc:String = "%"
                    let hum:String = String(format: "%.0f", humidity)
                    self._humidity = "\(hum) \(perc)"
//                    self._rain = String(format: "%.0f mm", rain)
                    self._pressure = String(format: "%.0f hPa", pressure)
                    self._windSpeed = String(format: "%.1f m/s", windSpeed)
                    self._windDegrees = String(format: "%.0f °", windDegrees)
                }
                completed()
        })
    }
}
