//
//  AppTBC.swift
//  
//
//  Created by Jeroen Dunselman on 20/12/2016.
//
//

import UIKit

class AppTBC: UITabBarController {
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //Refresh weather info when active tab changes
        appDelegate.reDetermineMyCurrentLocation(itemTitle: item.title!)
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
