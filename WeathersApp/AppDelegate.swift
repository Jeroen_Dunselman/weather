//
//  AppDelegate.swift
//
//  Created by Jeroen Dunselman on 17/12/2016.
//  Copyright © 2016 Jeroen Dunselman. All rights reserved.
//

import UIKit
import CoreLocation
//import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, CLLocationManagerDelegate, UITabBarControllerDelegate {
    
    var locationManager:CLLocationManager!
    var status : CLAuthorizationStatus = CLLocationManager.authorizationStatus()
    var latitudeCurrent : String?
    var longitudeCurrent : String?
    var flagLocaReceived = false
    
    var currentWeather = CurrentWeatherData()
    var forecastWeather = ForecastWeatherData()
    var requestingWeatherData : String?
    
    var window: UIWindow?
    var tabTodayVC : TodayVC?
    var tabForecastVC : ForecastVC?
    
    let tabBarController = AppTBC()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        /* [START tracker_swift]
         FIRApp.configure()[END tracker_swift]
         */
        
        //set active report type
        requestingWeatherData = "Today"
        
        //we have the right authorization
        //start the gps
        determineMyCurrentLocation()
        
        //creating both views
        self.tabTodayVC = TodayVC()
        self.tabForecastVC = ForecastVC()
        
        //give both views a handle to weather data
        self.tabTodayVC?.weather = self.currentWeather
        self.tabForecastVC?.weather = self.forecastWeather
        
        //adding both views to tabbar
        tabBarController.viewControllers = [tabTodayVC! , tabForecastVC!]
        
        //config and add the tabbar items
        var item = UITabBarItem(title: "Today",
                                image: UIImage(named: "Today"), tag: 0)
        tabTodayVC?.tabBarItem = item
        item = UITabBarItem(title: "Forecast",
                            image: UIImage(named: "Forecast_blue"), tag: 1)
        tabForecastVC?.tabBarItem = item
        
//        //let me handle tabBar didSelectItem events
        tabBarController.delegate = self
        
        //setting the initial ViewController as tabBarController
        self.window?.rootViewController = tabBarController
        window?.makeKeyAndVisible()
        
        //warn if app can't use gps
        
        if status == CLAuthorizationStatus.denied ||
            status == CLAuthorizationStatus.restricted
        {
            self.explainAuthorization()
        }
        
        
        return true
    }
    
    func refreshData() {
//        MARK: - issue forecastReport data not reloading when fetched seperately from currentReport
//        if String(self.requestingWeatherData!) == "Today" {
            currentWeather.setCoordinates(lat: self.latitudeCurrent!,
                                          lon: self.longitudeCurrent!)
            currentWeather.downloadData{
                //            print("fetched data currentWeather")
                self.tabTodayVC?.updateUI()
            }
//        } else if String(self.requestingWeatherData!) == "Forecast" {
            forecastWeather.setCoordinates(lat: self.latitudeCurrent!,
                                           lon: self.longitudeCurrent!)
            forecastWeather.downloadData{
                //            print("fetched data forecastWeather")
                self.tabForecastVC?.updateUI()
            }
//        }
        
//        MARK: - todo:register data
        /* register location and temperature
         let tempRegistered = self.tabTodayVC?.weather?.temp
         let latRegistered = self.latitudeCurrent!
         // [START user_property]
         FIRAnalytics.setUserPropertyString(temp, forName: "favorite_food")
         // [END user_property]
         */
    }
    
    func determineMyCurrentLocation() {
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            // MARK: - todo: use self.locationManager.requestLocation()
            locationManager.startUpdatingLocation()
        }
    }
    
    func reDetermineMyCurrentLocation(itemTitle:String) {
        flagLocaReceived = false
        self.requestingWeatherData = itemTitle
        determineMyCurrentLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let userLocation:CLLocation = locations[0] as CLLocation
        
        if (!flagLocaReceived) {
            locationManager.stopUpdatingLocation()
            
            //MARK: - number of decimals in coordinates determines app data definition depth
            //let accuracy:Int = 2; let accFormat:String = "%.\(accuracy)f"
            
            let latFound:String = String(format: "%.1f", userLocation.coordinate.latitude)
            let lonFound:String = String(format: "%.1f", userLocation.coordinate.longitude)
            
            if (latitudeCurrent != latFound
                ||
                longitudeCurrent != lonFound)
            {
                //save current location
                latitudeCurrent = latFound
                longitudeCurrent = lonFound
            }
            
            self.refreshData()
            
            //prevent any more location updates
            flagLocaReceived = true;

        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error)
    {
//        print("Error \(error)")
    }
    
    func explainAuthorization() {
//        MARK: - issue not showing Alertview

        let alertController = UIAlertController(title: "Destructive", message: "Simple alertView demo with Destructive and Ok.", preferredStyle: UIAlertControllerStyle.alert) //Replace UIAlertControllerStyle.Alert by UIAlertControllerStyle.alert
        let DestructiveAction = UIAlertAction(title: "Destructive", style: UIAlertActionStyle.destructive) { (result : UIAlertAction) -> Void in
            print("Destructive")
        }
        // Replace UIAlertActionStyle.Default by UIAlertActionStyle.default
        let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default) { (result : UIAlertAction) -> Void in
            print("OK")
        }
        alertController.addAction(DestructiveAction)
        alertController.addAction(okAction)
        tabBarController.presentedViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        determineMyCurrentLocation()
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
}
