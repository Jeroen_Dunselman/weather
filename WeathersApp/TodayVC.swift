//
//  TodayVC.swift
//  WhatsTheWeather
//
//  Created by Jeroen Dunselman on 16/12/2016.
//  Copyright © 2016 Jeroen Dunselman. All rights reserved.
//

import UIKit

class TodayVC: UIViewController {
    //gets data from openweathermap
    var weather : CurrentWeatherData?
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var weatherLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var rainLabel: UILabel!
    @IBOutlet weak var pressureLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var windDirectionLabel: UILabel!
    @IBAction func shareButtonClicked(sender: UIButton) {
        let textToShare = "Looking at WeathersApp, an awesome app by Jeroen Dunselman for STRV!"
        
        if let myWebsite = NSURL(string: "http://www.strv.com/") {
            let objectsToShare = [textToShare, myWebsite] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            activityVC.popoverPresentationController?.sourceView = sender
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    
    var activityIndicator:UIActivityIndicatorView = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //triggered by appdelegate when new data is available
    func updateUI() {
        windLabel.text = weather?.windSpeed
        windDirectionLabel.text = weather?.windDegrees
        rainLabel.text = weather?.rain
        tempLabel.text = weather?.temp//"\(weather?.temp)"
        locationLabel.text = weather?.location
        
        humidityLabel.text = weather?.humidity
        pressureLabel.text = weather?.pressure
        
        //pick icon to show for weathertype of day
        let weatherText:String  = (weather?.weather)!
        weatherLabel.text = weatherText
        switch weatherText {
        case "Rain", "Storm", "Snow", "Extreme", "Sunstorm":
            weatherImage.image = Icon.rain.image
        case "Clear", "Fish Rain":
            weatherImage.image = Icon.clear.image
        case "Clouds", "Mist", "Nebulae":
            weatherImage.image = Icon.clouds.image
        default:
            weatherImage.image = Icon.sun.image
        }
        
        // MARK: - Issue UIActivityIndicatorView not showing
        //        showActivityIndicatory(uiView: self.view)
    }
    enum Icon {
        case clear
        case clouds
        case rain
        case sun
        
        /*https://openweathermap.desk.com/customer/portal/questions/8623133-full-list-of-main-weather-conditions?t=535697
         "The conditions in ['weather'][0]['main'] array.Those I know about are:
         [Clear        Mist        Rain        Storm, Snow,Extreme]"*/
        
        var image: UIImage {
            switch self {
            case .clear: return UIImage(named: "CL")!
            case .clouds: return UIImage(named: "CS")!
            case .rain: return UIImage(named: "Rain")!
            case .sun: return UIImage(named: "Sun_Big")!
            }
        }
    }
    
    //MARK: - issue not visible
    func showActivityIndicatory(uiView: UIView) {
        let actInd: UIActivityIndicatorView = UIActivityIndicatorView()
        let rect:CGRect = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: 40, height: 40))
        actInd.frame = rect
        actInd.center = uiView.center
        actInd.hidesWhenStopped = true
        //        actInd.activityIndicatorViewStyle =
        //            UIActivityIndicatorViewStyle.WhiteLarge
        uiView.addSubview(actInd)
        activityIndicator.startAnimating()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
