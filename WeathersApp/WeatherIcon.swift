//
//  WeatherIcon.swift
//  WeathersApp
//
//  Created by Jeroen Dunselman on 19/12/2016.
//  Copyright © 2016 Jeroen Dunselman. All rights reserved.
//

import Foundation
import UIKit
enum Icon {
    case clear
    case clouds
    case rain
    case sun
    
    /*https://openweathermap.desk.com/customer/portal/questions/8623133-full-list-of-main-weather-conditions?t=535697
     "The conditions in ['weather'][0]['main'] array.Those I know about are:
     [Clear        Mist        Rain        Storm, Snow,Extreme]"*/
    
    var image: UIImage {
        switch self {
        case .clear: return UIImage(named: "CL")!
        case .clouds: return UIImage(named: "CS")!
        case .rain: return UIImage(named: "Rain")!
        case .sun: return UIImage(named: "Sun_Big")!
        }
        
    }
}
