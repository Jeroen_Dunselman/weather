//
//  ForecastVC.swift
//
//  Created by Jeroen Dunselman on 19/12/2016.
//  Copyright © 2016 Jeroen Dunselman. All rights reserved.
//

import UIKit

extension Date {
    //returns local language weekday
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }
}

class ForecastVC: UIViewController, UITableViewDelegate, UITableViewDataSource   {
    @IBOutlet weak var cityName: UILabel!
    @IBOutlet weak var tableSubView: UIView!
    var myTableView:UITableView!
    var weather : ForecastWeatherData?
    var weatherData: Array<NSDictionary>? //[NSDictionary] = Array()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        weatherData = (weather?.weatherData)!
        cityName.text = (weather?.cityName)!
        myTableView = UITableView(frame: tableSubView.bounds, style: UITableViewStyle.grouped)
        myTableView.register(UINib(nibName: "ForecastCell", bundle: nil), forCellReuseIdentifier: "dayCell")
        
        myTableView.delegate = self
        myTableView.dataSource = self
        self.tableSubView.addSubview(myTableView)
    }
    
    //triggered by appdelegate when new data is available
    func updateUI() {
        self.myTableView.reloadData()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath) as? ForecastCell
        let dayRow : Int = indexPath.row
        
        // Configure the cell...
        //Get date of upcoming days
        let today : Date = Date() // as NSDate
        let minutesToNextDays : Int = dayRow*60*60*24
        let upcomingDay = today.addingTimeInterval(TimeInterval(minutesToNextDays))
        //Show day name
        cell?.dayLabel.text = upcomingDay.dayOfWeek()!
        
        //show text weather of day
        if (self.weatherData?.count)! > 0 {
        let day = (self.weatherData?[dayRow])! as NSDictionary
        cell?.weatherLabel.text = day["Desc"] as? String
        cell?.tempLabel.text = (day["Temp"] as? String)
        
        //pick icon to show for weathertype of day
        let weatherType : String = (day["Type"] as? String)!
        switch weatherType {
        case "Rain", "Storm", "Snow":
            cell?.weatherImage.image = Icon.rain.image
        case "Clear":
            cell?.weatherImage.image = Icon.clear.image
        case "Clouds":
            cell?.weatherImage.image = Icon.clouds.image
        default:
            cell?.weatherImage.image = Icon.sun.image
        }
        }
        return cell!
    }
    
    enum Icon {
        case clear
        case clouds
        case rain
        case sun
        
        /*https://openweathermap.desk.com/customer/portal/questions/8623133-full-list-of-main-weather-conditions?t=535697
         "The conditions in ['weather'][0]['main'] array.Those I know about are:
         [Clear        Mist        Rain        Storm, Snow,Extreme]"*/
        
        var image: UIImage {
            switch self {
            case .clear: return UIImage(named: "CL")!
            case .clouds: return UIImage(named: "CS")!
            case .rain: return UIImage(named: "Rain")!
            case .sun: return UIImage(named: "Sun_Big")!
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 72.0;//Choose your custom row height
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
